FROM ubuntu:focal
ENV DEBIAN_FRONTEND=noninteractive
ARG SOURCEDIR
ADD deps /app/deps
RUN dpkg -i /app/deps/libbcg729*
RUN apt-get update && apt-get install -qy dpkg-dev devscripts equivs
ADD $SOURCEDIR /app/src/$SOURCEDIR
ADD patches /app/src/patches
WORKDIR /app/src/$SOURCEDIR
RUN sed -i 's,iptables-dev,libxtables-dev,' /app/src/$SOURCEDIR/debian/control
RUN for patch in /app/src/patches/*; do patch -p0 < $patch || exit 1; done
RUN DEBEMAIL=ops@cloudonix.io DEBFULLNAME="Cloudonix Operations" dch -D unstable -l-cx -U 'Build with experimental zero-knowledge-scaling'
RUN DEBEMAIL=ops@cloudonix.io DEBFULLNAME="Cloudonix Operations" dch -D unstable -l-cx -U 'fixed re-offer handling'
RUN DEBEMAIL=ops@cloudonix.io DEBFULLNAME="Cloudonix Operations" dch -D unstable -l-cx -U 'removed old clustering behavior of "restart on second offer"'
RUN DEBEMAIL=ops@cloudonix.io DEBFULLNAME="Cloudonix Operations" dch -D unstable -l-cx -U 'hack foreign server address into ice candidates, instead of doing the right thing'
RUN mk-build-deps --install --tool='apt-get -o Debug::pkgProblemResolver=yes --no-install-recommends --yes' debian/control
RUN rm *build-deps*.deb
RUN dpkg-buildpackage -us -uc
