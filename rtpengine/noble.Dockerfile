FROM ubuntu:noble
ENV DEBIAN_FRONTEND=noninteractive
ARG SOURCEDIR
RUN apt-get update && apt-get install -qy dpkg-dev devscripts equivs libbcg729-dev
ADD $SOURCEDIR /app/src/$SOURCEDIR
ADD patches /app/src/patches
WORKDIR /app/src/$SOURCEDIR
#RUN for patch in /app/src/patches/*; do patch -p0 < $patch || exit 1; done
RUN mk-build-deps --install --tool='apt-get -o Debug::pkgProblemResolver=yes --no-install-recommends --yes' debian/control
RUN rm *build-deps*.deb
RUN dpkg-buildpackage -us -uc
