FROM ubuntu:jammy
RUN echo 'deb [trusted=yes] file:/repo ./' >> /etc/apt/sources.list.d/local.list
ADD ./ /repo
#RUN --mount=type=cache,target=/var/cache/apt \
#	--mount=type=cache,target=/var/lib/apt \
RUN apt update && apt install -qy ngcp-rtpengine-daemon \
	&& rm -rf /var/cache/apt/archives /var/lib/apt/lists
CMD [ "/usr/bin/rtpengine", "-t", "-1", "-f", "-E", "--no-log-timestamps", "--pidfile", "/run/ngcp-rtpengine-daemon.pid", "--config-file", "/etc/rtpengine/rtpengine.conf" ]
