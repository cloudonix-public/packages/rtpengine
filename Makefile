ifeq (, $(shell type podman))
DOCKER      := docker
DOCKER_BUILD := docker
else
DOCKER      := podman
#DOCKER_BUILD := buildah
DOCKER_BUILD := podman
endif

all: repo

repo: repo/jammy repo/noble

images: image/jammy image/noble
	$(DOCKER) tag quay.io/cloudonix/rtpengine:noble quay.io/cloudonix/rtpengine:latest

repo/%: rtpengine/deb/%
	mkdir -p $@/binary-amd64
	$(MAKE) rtpengine/deb/$*
	cp rtpengine/deb/$*/* $@/binary-amd64/
	$(DOCKER) run -a stdout -a stderr --rm \
		-v $(shell pwd)/$@:/$@ -w /$@ ubuntu:$* bash -exc '\
		apt-get update && apt-get install -qy dpkg-dev; \
		ls binary-amd64/ | grep deb | cut -d_ -f1 | sort -u | while read pkg; do echo "$pkg required net"; done > override.conf; \
		dpkg-scanpackages . override.conf > Release; \
		dpkg-scanpackages binary-amd64 override.conf > Packages && gzip -9c < Packages > Packages.gz; \
	'

rtpengine/deb/%:
	$(MAKE) -C $(dir $(@D)) deb/$*

image/%: repo/%
	$(DOCKER_BUILD) build -t quay.io/cloudonix/rtpengine:$* -f containers/$*.Dockerfile repo/$*
	$(DOCKER) run -ti --rm quay.io/cloudonix/rtpengine:$* dpkg-query --show --showformat='$${Version}' ngcp-rtpengine-daemon | sed 's/.*mr//' > .version
	set -x; while [ `stat -c '%s' .version` -gt 0 ]; do \
		$(DOCKER) tag quay.io/cloudonix/rtpengine:$* quay.io/cloudonix/rtpengine:`cat .version`_$*; \
		sed -i -E 's/.?[0-9]+$$//;' .version; \
	done
	
clean:
	$(MAKE) -C rtpengine clean
	rm -rf repo .version

.PHONY: all repo image image/% clean
